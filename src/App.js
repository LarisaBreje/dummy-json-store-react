import Home from './pages/home';
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import About from './pages/about';
import Details from './pages/details';
import UserDetails from './pages/UserDetails';
import Users from './pages/Users';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';

function App() {
  const [searchData, setSearchData] = useState();

  const loadData = searchTerm => {
    setSearchData(searchTerm);
    console.log(searchTerm);
  };

  useEffect(() => {
  }, [searchData]);

  return (
    <Router>
      <Navbar onSearch={loadData} />
      <Routes>
        <Route index element={<Home searchData={searchData} />} />
        <Route path="/Users" element={<Users />} />
        <Route path="/about" element={<About />} />
        <Route path="/UserDetails/:userId" element={<UserDetails />} />
        <Route path="/details/:productId" element={<Details />} />
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;
