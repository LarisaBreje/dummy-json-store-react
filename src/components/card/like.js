import React, { useState } from 'react';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';

const Like = ({color}) => {
  const [like, setLike] = useState(false);

  return (
    <div className='heart-icon'>
      {like && (
        <AiFillHeart
          color={color}
          onClick={() => {
            setLike(false);
          }}
        />
      )}
      {!like && (
        <AiOutlineHeart
          color={color}
          onClick={() => {
            setLike(true);
          }}
        />
      )}
    </div>
  );
};

export default Like;
