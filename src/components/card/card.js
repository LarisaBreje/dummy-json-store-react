import './card.css';
import Like from './like';

const Card = ({ data, onClick }) => {
  return (
    <div className="card-container">
      <div>
        <img src={data.thumbnail} alt="product" />
        <div className="card-title">
          <strong>{data.title}</strong>
          <Like color="#DF3050" />
        </div>
        <p className="card-details">{data.price}$</p>
        <p className="card-details">{data.description}</p>
      </div>
        <button className="card-btn" onClick={() => onClick(data.id)}>
          <strong>See more pictures</strong>
        </button>
      </div>
  );
};

export default Card;
