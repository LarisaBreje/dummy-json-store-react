import './footer.css';

const Footer = () => {
  return (
    <div className="footer">
      <footer>Copyright © 2023 Larisa Breje</footer>
    </div>
  );
};

export default Footer;
