import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import './navbar.css';
import { GiVintageRobot } from 'react-icons/gi';
import { FiSearch } from 'react-icons/fi';
import { AiOutlineHome } from 'react-icons/ai';
import { HiOutlineUsers } from 'react-icons/hi';
import { HiOutlineInformationCircle } from 'react-icons/hi';

const Navbar = ({ onSearch }) => {
  const [search, setSearch] = useState();

  const handleKeyPress = e => {
    if (e.key === 'Enter') {
      onSearch({ search });
    }
  };
  return (
    <div className="navbar">
      <div className="navbar-title">
        <span class="robot-icon">
          <GiVintageRobot />
        </span>
        <h3>DummyJSON Store</h3>
      </div>
      <div className="search-container">
        <input
          type="text"
          className="search"
          placeholder="Search"
          value={search}
          onChange={e => {
            setSearch(e.target.value);
            // onSearch({ search });
          }}
          onKeyDown={handleKeyPress}
        />
        <FiSearch
          className="search-icon"
          onClick={() => onSearch({ search })}
        />
      </div>
      <div className="links">
        <div class="home-icon">
          <AiOutlineHome />
          <Link to="/">Home</Link>
        </div>
        <div class="users-icon">
          <HiOutlineUsers />
          <Link to="/Users">Users</Link>
        </div>
        <div class="about-icon">
          <HiOutlineInformationCircle />
          <Link to="/about">About</Link>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
