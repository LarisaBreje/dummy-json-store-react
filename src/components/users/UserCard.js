import '../users/userCard.css';

const UserCard = ({ data, onClick }) => {
  return (
    <div className="user-card-container" onClick={() => onClick(data.id)}>
      <div className="user-card-info">
        <img src={data.image} alt="user" />
        <div>
          {' '}
          <ul>
            <li>
              Name: <strong>{data.firstName + ' ' + data.lastName}</strong>
            </li>
            <li>Age: {data.age}</li>
            <li>Email: {data.email}</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default UserCard;


