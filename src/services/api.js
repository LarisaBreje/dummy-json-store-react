import axios from 'axios';

const api = axios.create({
  baseURL: 'https://dummyjson.com',
  headers: { Accept: 'application/json' },
});

const getProducts = searchTerm => {

  if (searchTerm) {
    return api.get(`/products/search?q=${searchTerm.search}`);
  }
  return api.get('/products');
};

const getProduct = productId => {
  return api.get(`/product/${productId}`);
};

const getUsers = () => {
  return api.get('/users');
};

const getUserById = userId => {
  return api.get(`/users/${userId}`);
};
export { getProducts, getProduct, getUsers, getUserById };
