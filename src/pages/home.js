import { useEffect, useState } from 'react';
import { getProducts } from '../services/api';
import Card from '../components/card/card';
import { useNavigate } from 'react-router-dom';

const Home = ({ searchData }) => {
  const [products, setProducts] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      try {
        const res = await getProducts(searchData);
        if (res.status === 200) setProducts(res.data.products);
      } catch (error) {
        console.log(error);
      }
    }
    fetchData();
  }, [searchData]);

  const handleOnClickCard = productID => {
    navigate(`/details/${productID}`);
  };

  return (
    <div className="home-container">
      {products &&
        products.map(product => (
          <Card className="" data={product} onClick={handleOnClickCard} />
        ))}
    </div>
  );
};

export default Home;
