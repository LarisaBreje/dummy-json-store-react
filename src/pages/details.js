import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getProduct } from "../services/api";

const Details = () => {

    const [details, setDetails] = useState();
    const { productId } = useParams();

    useEffect(() => {
        async function fetchData() {
            try{
                const res = await getProduct(productId);
                if(res.status === 200)setDetails(res.data)
            }
            catch(error){
                console.log(error)
            }
        }
        fetchData()

        // getProducts()
        // .then(res => {
        //     console.log(res)
        // })
        // .catch(error => {
        //     console.log(error)
        // })
    }, [productId])

    return(
        <div>
            <h1>{details?.title}</h1>
            {details?.images.map(image => (
                <img src={image} alt="product"/>
            ))}
        </div>
    )
}

export default Details;