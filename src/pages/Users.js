import { useState, useEffect } from 'react';
import { getUsers } from '../services/api';
import { useNavigate } from 'react-router-dom';
import UserCard from '../components/users/UserCard';
import '../components/users/users.css';

const Users = () => {

  const [users, setUsers] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      try {
        const res = await getUsers();
        if (res.status === 200) {
          setUsers(res.data.users);
        }
      } catch (error) {
        alert(error.message);
      }
    }
    fetchData();
  }, []);

  const handleClick = (userId) => {
    navigate(`/UserDetails/${userId}`);
  };

  return (
    <div className="users-container">
      {users &&
        users.map(user => <UserCard data={user} onClick={handleClick} />)}
    </div>
  );
};

export default Users;
