import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getUserById } from '../services/api';
import '../components/users/userDetails.css';

const UserDetails = () => {
  const [details, setDetails] = useState();
  const { userId } = useParams();

  useEffect(() => {
    async function fetchData() {
      try {
        const res = await getUserById(userId);
        if (res.status === 200) setDetails(res.data);
      } catch (error) {
        console.log(error);
      }
    }
    fetchData();
  }, [userId]);

  return (
    <div className="user-details-container">
      <h1>
        {details?.firstName} {details?.lastName}
      </h1>
      <ul>
        <strong>Address:</strong>
        <br></br>
        <br />
        <li>Street: {details?.address.address}</li>
        <li>City: {details?.address.city}</li>
        <li>State: {details?.address.state}</li>
        <li>Postal code: {details?.address.postalCode}</li>{' '}
      </ul>
      <img src={details?.image} alt="user" />
    </div>
  );
};

export default UserDetails;
